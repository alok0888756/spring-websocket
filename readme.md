# Springboot Chat Application

This is a messaging application built with Spring Boot, WebSocket, Redis, Kafka, and PostgreSQL.

Overview
This application allows users to send and receive real-time messages. 
It leverages WebSocket for bidirectional communication, Redis for caching user 
information, Kafka for message streaming, and PostgreSQL for data persistence.

## Technologies Used
* Spring Boot: Framework for building Java-based applications.
* WebSocket: Protocol for full-duplex communication channels over a single TCP connection.
* Redis: In-memory data structure store used for caching current user information.
* Kafka: Distributed streaming platform for publishing and subscribing to streams of records.
* PostgreSQL: Relational database used for storing user and message information.

## Local Setup

Follow these steps to set up and run the messaging application locally:

### Prerequisites
* Java Development Kit (JDK) 17 or higher
* Apache Maven
* Redis (can be installed locally or accessed via a remote server)
* Apache Kafka (can be installed locally or use docker image of it)
* PostgreSQL database



### Steps

Clone the Repository

* git clone https://gitlab.com/alok0888756/spring-websocket.git
* Configure Redis and Kafka Ensure that Redis and Kafka are running and accessible. 
Update the application yml with the appropriate connection details.
* Set Up PostgreSQL DatabaseCreate a PostgreSQL database named messaging_app and configure the database connection in the application properties.
* Build and Run the ApplicationNavigate to the project directory and build the application using Maven:

* cd spring-websocket
* mvn clean package
* mvn spring-boot:run
* Access the ApplicationOnce the application is running locally, you can access it using the following URL:
http://localhost:4545