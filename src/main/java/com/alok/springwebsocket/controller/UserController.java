package com.alok.springwebsocket.controller;

import com.alok.springwebsocket.service.UserService;
import com.alok.springwebsocket.models.User;
import com.alok.springwebsocket.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/users")
public class UserController {
    @Autowired
    private UserRepository userRepository;


    @Autowired
    private UserService userService;

    @PostMapping("/send-message")
    public ResponseEntity<?> sendMessage(@RequestBody String message) {
        userService.sendMessage(message);
        return ResponseEntity.ok("Message sent successfully");
    }
    @PostMapping("/register")
    public ResponseEntity<?> registerUser(@RequestBody User newUser) {
        // Check if username is already taken
        if (userRepository.findByUsername(newUser.getUsername()) != null) {
            return ResponseEntity.badRequest().body("Username is already taken");
        }
        // Save new user to database
        User savedUser = userRepository.save(newUser);
        return ResponseEntity.ok(savedUser);
    }
}
