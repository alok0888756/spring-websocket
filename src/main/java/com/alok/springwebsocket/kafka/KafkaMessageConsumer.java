package com.alok.springwebsocket.kafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class KafkaMessageConsumer {

    @Autowired
    private RedisMessagePublisher redisMessagePublisher;

    @KafkaListener(topics = "chat-messages", groupId = "my-group")
    public void listen(String message) {
        // Process the received message (e.g., store in Redis)
        redisMessagePublisher.publish(message);
    }
}
