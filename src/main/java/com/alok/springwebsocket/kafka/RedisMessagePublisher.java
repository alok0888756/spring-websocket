package com.alok.springwebsocket.kafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

@Component
public class RedisMessagePublisher {

    private static final String REDIS_TOPIC = "chat-history";

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    public void publish(String message) {
        stringRedisTemplate.opsForList().leftPush(REDIS_TOPIC, message);
    }
}
