package com.alok.springwebsocket.service;

import com.alok.springwebsocket.kafka.KafkaMessageProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private KafkaMessageProducer kafkaMessageProducer;

    public void sendMessage(String message) {
        // Send message to Kafka
        kafkaMessageProducer.sendMessage(message);
    }
}