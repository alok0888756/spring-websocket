package com.alok.springwebsocket.config;

import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.messaging.support.ExecutorSubscribableChannel;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebMvcStompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebMvcStompWebSocketEndpointRegistration;
import org.springframework.web.socket.handler.BinaryWebSocketHandler;

@ContextConfiguration(classes = {Config.class})
@ExtendWith(SpringExtension.class)
class ConfigTest {
    @Autowired
    private Config config;

    /**
     * Method under test:
     * {@link Config#registerStompEndpoints(StompEndpointRegistry)}
     */
    @Test
    void testRegisterStompEndpoints() {
        // Arrange
        WebMvcStompEndpointRegistry registry = mock(WebMvcStompEndpointRegistry.class);
        BinaryWebSocketHandler webSocketHandler = new BinaryWebSocketHandler();
        when(registry.addEndpoint(isA(String[].class))).thenReturn(new WebMvcStompWebSocketEndpointRegistration(
                new String[]{"Paths"}, webSocketHandler, new ConcurrentTaskScheduler()));

        // Act
        config.registerStompEndpoints(registry);

        // Assert
        verify(registry).addEndpoint(isA(String[].class));
    }

    /**
     * Method under test:
     * {@link Config#configureMessageBroker(MessageBrokerRegistry)}
     */
    @Test
    @Disabled("TODO: Complete this test")
    void testConfigureMessageBroker() {
        // Arrange and Act
        config.configureMessageBroker(null);
    }

    /**
     * Method under test:
     * {@link Config#configureMessageBroker(MessageBrokerRegistry)}
     */
    @Test
    void testConfigureMessageBroker2() {
        // TODO: Diffblue Cover was only able to create a partial test for this method:
        //   Reason: Missing observers.
        //   Diffblue Cover was unable to create an assertion.
        //   Add getters for the following fields or make them package-private:
        //     MessageBrokerRegistry.applicationDestinationPrefixes
        //     MessageBrokerRegistry.brokerChannelRegistration
        //     MessageBrokerRegistry.brokerRelayRegistration
        //     MessageBrokerRegistry.cacheLimit
        //     MessageBrokerRegistry.clientInboundChannel
        //     MessageBrokerRegistry.clientOutboundChannel
        //     MessageBrokerRegistry.pathMatcher
        //     MessageBrokerRegistry.preservePublishOrder
        //     MessageBrokerRegistry.simpleBrokerRegistration
        //     MessageBrokerRegistry.userDestinationPrefix
        //     MessageBrokerRegistry.userRegistryOrder

        // Arrange and Act
        config.configureMessageBroker(
                new MessageBrokerRegistry(new ExecutorSubscribableChannel(), mock(MessageChannel.class)));
    }
}
