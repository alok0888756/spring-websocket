package com.alok.springwebsocket.controller;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.Mockito.mock;

import com.alok.springwebsocket.models.Message;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {MessageController.class})
@ExtendWith(SpringExtension.class)
class MessageControllerTest {
    @Autowired
    private MessageController messageController;

    /**
     * Method under test: {@link MessageController#getContent(Message)}
     */
    @Test
    void testGetContent() {
        // Arrange
        MessageController messageController2 = new MessageController();
        Message message = new Message("Name", "Not all who wander are lost");

        // Act and Assert
        assertSame(message, messageController2.getContent(message));
    }

    /**
     * Method under test: {@link MessageController#getContent(Message)}
     */
    @Test
    void testGetContent2() {
        // Arrange
        Message message = mock(Message.class);

        // Act and Assert
        assertSame(message, (new MessageController()).getContent(message));
    }
}
