package com.alok.springwebsocket.controller;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import com.alok.springwebsocket.models.User;
import com.alok.springwebsocket.repository.UserRepository;
import com.alok.springwebsocket.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.aot.DisabledInAotMode;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@ContextConfiguration(classes = {UserController.class})
@ExtendWith(SpringExtension.class)
@DisabledInAotMode
class UserControllerTest {
    @Autowired
    private UserController userController;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private UserService userService;

    /**
     * Method under test: {@link UserController#registerUser(User)}
     */
    @Test
    void testRegisterUser() throws Exception {
        // Arrange
        User user = new User();
        user.setId(1L);
        user.setPassword("iloveyou");
        user.setUsername("janedoe");

        User user2 = new User();
        user2.setId(1L);
        user2.setPassword("iloveyou");
        user2.setUsername("janedoe");
        when(userRepository.findByUsername(Mockito.<String>any())).thenReturn(user);
        when(userRepository.save(Mockito.<User>any())).thenReturn(user2);

        User user3 = new User();
        user3.setId(1L);
        user3.setPassword("iloveyou");
        user3.setUsername("janedoe");
        String content = (new ObjectMapper()).writeValueAsString(user3);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/users/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);

        // Act
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(userController).build().perform(requestBuilder);

        // Assert
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(400))
                .andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=ISO-8859-1"))
                .andExpect(MockMvcResultMatchers.content().string("Username is already taken"));
    }

    /**
     * Method under test: {@link UserController#sendMessage(String)}
     */
    @Test
    void testSendMessage() throws Exception {
        // Arrange
        doNothing().when(userService).sendMessage(Mockito.<String>any());
        MockHttpServletRequestBuilder contentTypeResult = MockMvcRequestBuilders.post("/api/users/send-message")
                .contentType(MediaType.APPLICATION_JSON);
        MockHttpServletRequestBuilder requestBuilder = contentTypeResult
                .content((new ObjectMapper()).writeValueAsString("foo"));

        // Act and Assert
        MockMvcBuilders.standaloneSetup(userController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=ISO-8859-1"))
                .andExpect(MockMvcResultMatchers.content().string("Message sent successfully"));
    }
}
