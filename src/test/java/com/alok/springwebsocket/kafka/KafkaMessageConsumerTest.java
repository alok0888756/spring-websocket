package com.alok.springwebsocket.kafka;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.aot.DisabledInAotMode;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {KafkaMessageConsumer.class})
@ExtendWith(SpringExtension.class)
@DisabledInAotMode
class KafkaMessageConsumerTest {
    @Autowired
    private KafkaMessageConsumer kafkaMessageConsumer;

    @MockBean
    private RedisMessagePublisher redisMessagePublisher;

    /**
     * Method under test: {@link KafkaMessageConsumer#listen(String)}
     */
    @Test
    void testListen() {
        // Arrange
        doNothing().when(redisMessagePublisher).publish(Mockito.<String>any());

        // Act
        kafkaMessageConsumer.listen("Not all who wander are lost");

        // Assert that nothing has changed
        verify(redisMessagePublisher).publish(eq("Not all who wander are lost"));
    }
}
