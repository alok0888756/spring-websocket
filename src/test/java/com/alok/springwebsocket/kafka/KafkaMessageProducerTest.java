package com.alok.springwebsocket.kafka;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.concurrent.CompletableFuture;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.aot.DisabledInAotMode;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {KafkaMessageProducer.class})
@ExtendWith(SpringExtension.class)
@DisabledInAotMode
class KafkaMessageProducerTest {
    @Autowired
    private KafkaMessageProducer kafkaMessageProducer;

    @MockBean
    private KafkaTemplate<String, String> kafkaTemplate;

    /**
     * Method under test: {@link KafkaMessageProducer#sendMessage(String)}
     */
    @Test
    void testSendMessage() {
        // Arrange
        when(kafkaTemplate.send(Mockito.<String>any(), Mockito.<String>any())).thenReturn(new CompletableFuture<>());

        // Act
        kafkaMessageProducer.sendMessage("Not all who wander are lost");

        // Assert that nothing has changed
        verify(kafkaTemplate).send(eq("chat-messages"), eq("Not all who wander are lost"));
    }
}
