package com.alok.springwebsocket.kafka;

import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.aot.DisabledInAotMode;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {RedisMessagePublisher.class})
@ExtendWith(SpringExtension.class)
@DisabledInAotMode
class RedisMessagePublisherTest {
    @Autowired
    private RedisMessagePublisher redisMessagePublisher;

    @MockBean
    private StringRedisTemplate stringRedisTemplate;

    /**
     * Method under test: {@link RedisMessagePublisher#publish(String)}
     */
    @Test
    @Disabled("TODO: Complete this test")
    void testPublish() {
        // Arrange
        when(stringRedisTemplate.opsForList()).thenReturn(null);

        // Act
        redisMessagePublisher.publish("Not all who wander are lost");
    }
}
