package com.alok.springwebsocket.models;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class MessageTest {
    /**
     * Methods under test:
     * <ul>
     *   <li>{@link Message#Message(String, String)}
     *   <li>{@link Message#setContent(String)}
     *   <li>{@link Message#setName(String)}
     *   <li>{@link Message#getContent()}
     *   <li>{@link Message#getName()}
     * </ul>
     */
    @Test
    void testGettersAndSetters() {
        // Arrange and Act
        Message actualMessage = new Message("Name", "Not all who wander are lost");
        actualMessage.setContent("Not all who wander are lost");
        actualMessage.setName("Name");
        String actualContent = actualMessage.getContent();

        // Assert that nothing has changed
        assertEquals("Name", actualMessage.getName());
        assertEquals("Not all who wander are lost", actualContent);
    }
}
