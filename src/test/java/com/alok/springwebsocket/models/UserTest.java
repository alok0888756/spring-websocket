package com.alok.springwebsocket.models;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class UserTest {
    /**
     * Methods under test:
     * <ul>
     *   <li>{@link User#User()}
     *   <li>{@link User#setId(Long)}
     *   <li>{@link User#setPassword(String)}
     *   <li>{@link User#setUsername(String)}
     *   <li>{@link User#toString()}
     *   <li>{@link User#getId()}
     *   <li>{@link User#getPassword()}
     *   <li>{@link User#getUsername()}
     * </ul>
     */
    @Test
    void testGettersAndSetters() {
        // Arrange and Act
        User actualUser = new User();
        actualUser.setId(1L);
        actualUser.setPassword("iloveyou");
        actualUser.setUsername("janedoe");
        String actualToStringResult = actualUser.toString();
        Long actualId = actualUser.getId();
        String actualPassword = actualUser.getPassword();

        // Assert that nothing has changed
        assertEquals("User{id=1, username='janedoe', password='iloveyou'}", actualToStringResult);
        assertEquals("iloveyou", actualPassword);
        assertEquals("janedoe", actualUser.getUsername());
        assertEquals(1L, actualId.longValue());
    }

    /**
     * Methods under test:
     * <ul>
     *   <li>{@link User#User(Long, String, String)}
     *   <li>{@link User#setId(Long)}
     *   <li>{@link User#setPassword(String)}
     *   <li>{@link User#setUsername(String)}
     *   <li>{@link User#toString()}
     *   <li>{@link User#getId()}
     *   <li>{@link User#getPassword()}
     *   <li>{@link User#getUsername()}
     * </ul>
     */
    @Test
    void testGettersAndSetters2() {
        // Arrange and Act
        User actualUser = new User(1L, "janedoe", "iloveyou");
        actualUser.setId(1L);
        actualUser.setPassword("iloveyou");
        actualUser.setUsername("janedoe");
        String actualToStringResult = actualUser.toString();
        Long actualId = actualUser.getId();
        String actualPassword = actualUser.getPassword();

        // Assert that nothing has changed
        assertEquals("User{id=1, username='janedoe', password='iloveyou'}", actualToStringResult);
        assertEquals("iloveyou", actualPassword);
        assertEquals("janedoe", actualUser.getUsername());
        assertEquals(1L, actualId.longValue());
    }
}
